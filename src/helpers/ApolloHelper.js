import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';

class ApolloHelper {

    static TOKEN = null;

    static initClient() {
        const authLink = setContext((_, { headers }) => {
            // return the headers to the context so httpLink can read them
            return {
                headers: {
                    ...headers,
                    authorization: this.TOKEN ? `Bearer ${this.TOKEN}` : '',
                }
            }
        });

        const httpLink = createHttpLink({
            uri: 'https://api.github.com/graphql',
        });

        const client = new ApolloClient({
            link: authLink.concat(httpLink),
            cache: new InMemoryCache(),
            dataIdFromObject: o => o.id
        });

        return client;
    }

    static setToken(token) {
        this.TOKEN = token; 
    }
}

export default ApolloHelper;