import React from 'react';

import Profile from './Profile';
import logo from '../logo.svg';


class Header extends React.Component {

	constructor(props) {
        super(props);
    }

	render() {
        return (
            <header className="App-header">
                <div className="container">
                    <h1 className="site-title">
                        <img src={logo} className="App-logo" alt="logo" />
                        Volodya test app
                    </h1>
                    <span className="site-tagline">
                        { this.props.isUserLoggedIn ? <Profile /> : ':3' }
                    </span>
                </div>
            </header>
        );
	}
}

export default Header;