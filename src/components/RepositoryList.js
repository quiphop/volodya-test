import React from 'react';

import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import GitHubLogin from './GitHubLogin';
import RepositoryItemWithMutations from './RepositoryItem';

const redirectUri = window.location.href;

const REPOS_QUERY = gql`{
    search(query: "language:JavaScript stars:>10000", type: REPOSITORY, first: 10) {
      edges {
        node {
          ... on Repository {
            id
            name
            descriptionHTML
            stargazers {
              totalCount
            }
            viewerHasStarred
          }
        }
      }
    }
}`;

const populate = graphql(REPOS_QUERY, {
    skip: props => !props.loginSuccess,
	props: ({  data: { loading, search }}) => ({
		loading, search
	})
});

class RepositoryList extends React.Component {

	constructor(props) {
        super(props);

        this.handleLoginSuccess = this.handleLoginSuccess.bind(this);
        this.handleLoginFailure = this.handleLoginFailure.bind(this);
    }

    handleLoginSuccess(response) {
        this.props.handleUserLogin(true);
    }

    handleLoginFailure(response) {
        this.props.handleUserLogin(false);
        console.error(response);
    }

	render() {
		const { loading, search } = this.props;

        if (!this.props.loginSuccess) {
            return (
                <div>
                    <h4>Please login first</h4>
                    <br/>
                    <GitHubLogin clientId="cd4cd56c702454842310"
                        onSuccess={ this.handleLoginSuccess }
                        onFailure={ this.handleLoginFailure }
                        redirectUri={ redirectUri }/>
                </div>
            );
        } else if (loading) {
			return (<h4><i className="fa fa-refresh load-icon"></i></h4>);
		} else {
            const reposList = search.edges.map((repo, index) => {
                return (
                    <RepositoryItemWithMutations repo={ repo } key={ index } />
                );
            });

            return (
                <ul className="repos-list">
                   { reposList }
                </ul>
            );
		}
	}
}

const PopulatedRepositoryList = populate(RepositoryList);

export default PopulatedRepositoryList;