import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ApolloHelper from '../helpers/ApolloHelper';
import PopupWindow from './PopupWindow';
import { toQuery } from '../helpers/utils';

class GitHubLogin extends Component {

    static propTypes = {
        buttonText: PropTypes.string,
        children: PropTypes.node,
        className: PropTypes.string,
        clientId: PropTypes.string.isRequired,
        onRequest: PropTypes.func,
        onSuccess: PropTypes.func,
        onFailure: PropTypes.func,
        redirectUri: PropTypes.string.isRequired,
        scope: PropTypes.string,
    }

	static defaultProps = {
		buttonText: 'GitHub SignIn',
		scope: 'repo,gist',
		onRequest: () => {},
		onSuccess: () => {},
		onFailure: () => {},
  	}

	handleClick = () => {
		const { clientId, scope, redirectUri } = this.props;
		const search = toQuery({
			client_id: clientId,
			scope,
            redirect_uri: redirectUri,
		});
		const popup = this.popup = PopupWindow.open(
			'github-oauth-authorize',
			`https://github.com/login/oauth/authorize?${search}`,
			{ height: 600, width: 600 }
		);

		this.onRequest();
		popup.then(
            data => this.onSuccess(data),
            error => this.onFailure(error)
		);
	}

    onRequest = () => {
        this.props.onRequest();
    }

    onSuccess = (data) => {
        if (!data.code) {
            return this.onFailure(new Error('\'code\' not found'));
        }

        this.getAccessToken(data);
    }

    onFailure = (error) => {
        this.props.onFailure(error);
    }

    getAccessToken(data) {
        const query = toQuery({
            code: data.code,
        });
        fetch(`http://localhost:3001/my-oauth?${query}`, {
            method: 'GET',
        }).then(response => response.json())
            .then(auth => {
                ApolloHelper.setToken(auth.access_token);
                this.props.onSuccess(auth.access_token);
            }
        );
    }

    render() {
        const { className, buttonText, children } = this.props;
        const attrs = { onClick: this.handleClick };

        if (className) {
            attrs.className = className;
        }

        return <button {...attrs} className="sign-in-button"><i className="fa fa-github-square"></i>{ children || buttonText }</button>;
    }
}

export default GitHubLogin;