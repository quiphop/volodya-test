import React from 'react';

import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const addStar = gql`mutation addStar($starrableId: ID!){
    addStar(input: { starrableId: $starrableId}) {
      clientMutationId
      starrable {
        stargazers {
          totalCount
        }
        viewerHasStarred
      }
    }
}`;


const removeStar = gql`mutation removeStar($starrableId: ID!){
    removeStar(input: { starrableId: $starrableId}) {
      clientMutationId
      starrable {
        stargazers {
          totalCount
        }
        viewerHasStarred
      }
    }
}`;

class RepositoryItem extends React.Component {

	constructor(props) {
        super(props);

        this.state = {
            repo: this.props.repo.node
        }
        this.handleStarClick = this.handleStarClick.bind(this);
    }

    handleStarClick(repo) {
        const onSuccess = (resp) => {
            // silly
            const key = resp.data.addStar ? 'addStar' : 'removeStar';
            const _repo = Object.assign(
                {},
                this.state.repo,
                {
                    stargazers: {
                        totalCount: resp.data[key].starrable.stargazers.totalCount
                    },
                    viewerHasStarred: resp.data[key].starrable.viewerHasStarred
                }
            );
            
            this.setState({repo: _repo});
        };

        const action = repo.viewerHasStarred ? this.props.removeStar : this.props.addStar;

        action({variables: { starrableId: repo.id }})
            .then(onSuccess);
    }

	render() {
		const { loading } = this.props;

		if (!loading) {
			return (
                <li key={ this.state.key }>
                    <h3>{ this.state.repo.name }</h3>
                    <p></p>
                    <p>stargazers: { this.state.repo.stargazers.totalCount }</p>
                    <button onClick={ () => this.handleStarClick(this.state.repo) }>
                        <i className={ this.state.repo.viewerHasStarred ? 'fa fa-star': 'fa fa-star-o' }></i> 
                        { this.state.repo.viewerHasStarred ? 'Unstar!' : 'Star!'}
                    </button>
                </li>
			)
		} else {
			return '';
		}
	}
}

const RepositoryItemWithMutations = 
    graphql(
        removeStar, {
            name : 'removeStar'
        }
    )
    (graphql(
        addStar, {
            name: 'addStar'
        }
    )(RepositoryItem)
);

export default RepositoryItemWithMutations;