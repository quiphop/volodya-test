import React, { Component } from 'react';

import { ApolloProvider } from 'react-apollo';
import ApolloHelper from './helpers/ApolloHelper';

import Footer from './components/Footer';
import PopulatedRepositoryList from './components/RepositoryList';
import './App.css';
import Header from './components/Header';

const client = ApolloHelper.initClient();


class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isUserLoggedIn: false
        };

        this.handleUserLogin = this.handleUserLogin.bind(this);
    }

    handleUserLogin(resp) {
        this.setState({ isUserLoggedIn: resp });
    }

    render() {
        return (
            <ApolloProvider client={client}>
                <div className="App">
                    <Header isUserLoggedIn={ this.state.isUserLoggedIn } />
                    <PopulatedRepositoryList
                        handleUserLogin={ this.handleUserLogin }
                        loginSuccess={ this.state.isUserLoggedIn }/>
                    <Footer/>
                </div>
            </ApolloProvider>
        );
    }
}

export default App;