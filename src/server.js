var axios = require('axios');
var express = require('express');
var app = express();

app.get('/my-oauth', function (req, res) {
  const GITHUB_AUTH_ACCESSTOKEN_URL = 'https://github.com/login/oauth/access_token'
  const CLIENT_ID = 'cd4cd56c702454842310'
  const CLIENT_SECRET = 'a5f178a37fce00e1f7d6319c14acb6f4ded5495c'
  const CODE = req.query.code
  const scopes = [
    'user',
    'public_repo',
    'repo',
  ]
  axios({
    method: 'post',
    url: GITHUB_AUTH_ACCESSTOKEN_URL,
    data: {
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      scopes: scopes,
      code: CODE,
      redirect_uri: 'http://localhost:3000/',
    },
    headers: {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
    }
  })
  .then(function (response) {
    res.setHeader('X-Frame-Options', 'ALLOWALL');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  	res.json(response.data);
  })
  .catch(function (error) {
    console.error('Error ' + error.message)
  })
});

app.listen(3001, function () {
  console.log('my-oauth listening on port 3001!');
});