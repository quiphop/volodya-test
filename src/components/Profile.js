import React from 'react';

import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const PROFILE_QUERY = gql`query {
    viewer {
        login
        name
        avatarUrl
    }
}`;

const withInfo = graphql(PROFILE_QUERY, {
	props: ({  data: { loading, viewer } }) => ({
		loading, viewer
	})
});

class Profile extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { loading, viewer } = this.props;

		if (!loading) {
			return (
				<div>
					<h4>
						<img src={ viewer.avatarUrl } className="avatar" alt="logo" />
						{viewer.name} aka {viewer.login}
					</h4>
				</div>
			)
		} else {
			return '';
		}
	}
}

const ProfileWithInfo = withInfo(Profile);

export default ProfileWithInfo;